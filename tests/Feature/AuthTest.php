<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use App\Models\User;
use Faker\Generator as Faker;

class AuthTest extends TestCase
{
    /** @test */

    public function tes_login()
    {
        $user       = User::factory()->make([
            'password' => Hash::make("123123"),
        ]);
        $user->save();

        $response = $this->post('/api/login',[
            "email"     => $user->email,
            "password"  => "123123",
        ]);

        if(isset($response["user"]) && isset($response["token"])){
            $response->assertStatus(200);
        }
    }


    /** @test */
    public function tes_logout()
    {
        $user       = User::factory()->make([
            'password' => Hash::make("123123"),
        ]);
        $user->save();

        $response = $this->post('/api/login',[
            "email"     => $user->email,
            "password"  => "123123",
        ]);
        
        $response  = $this->post("/api/logout",[],['HTTP_Authorization' => 'Bearer' . $response["token"]]);

        $response->assertStatus(200);
    }
}
