<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use App\Models\User;
use App\Models\Kendaraan;

class PenjualanTest extends TestCase
{
    public $mobil = null;
    public $motor = null;
    public $token = null;

    public function setUp() :void{
        parent::setUp();
        $user       = User::factory()->make([
            'password' => Hash::make("123123"),
        ]);
        $user->save();

        $response = $this->post('/api/login',[
            "email"     => $user->email,
            "password"  => "123123",
        ]);

        $this->mobil = Kendaraan::create(["tahun"=> 2008, "warna" => "silver", "harga" => 1000000, "mesin" => "VVTi", "kapasitas" => 6 , "tipe" => "SUV", "stok"=> 30]);
        $this->motor = Kendaraan::create(["tahun"=> 2020, "warna" => "putih", "harga" => 10000, "mesin" => "125cc", "tipe_suspensi" => "mono" , "tipe_transmisi" => "matic", "stok"=> 50]);
        $this->token = $response["token"];
    }

    public function test_insert_penjualan_mobil(){

        $response = $this->post('/api/penjualan/store',[
            "kendaraan" => $this->mobil->_id,
            "jumlah"    => 10,
            "pembeli"   => "nama",
            "tgl_pembelian" => date('Y-m-d'),
        ],
        ['HTTP_Authorization' => 'Bearer'.$this->token]);
        
        $response->assertJsonStructure(["data", "message"]);
        $response->assertStatus(200);

        $this->assertDatabaseHas("penjualan",["_id"=>$response["data"]["_id"]]);
    }

    public function test_insert_penjualan_motor(){
        $response = $this->post('/api/penjualan/store',[
            "kendaraan" => $this->motor->_id,
            "jumlah"    => 10,
            "pembeli"   => "nama",
            "tgl_pembelian" => date('Y-m-d'),
        ],
        ['HTTP_Authorization' => 'Bearer'.$this->token]);
        
        $response->assertJsonStructure(["data", "message"]);
        $response->assertStatus(200);

        $this->assertDatabaseHas("penjualan",["_id"=>$response["data"]["_id"]]);
    }

    public function test_report()
    {
        $response = $this->get('/api/penjualan/report',['HTTP_Authorization' => 'Bearer'.$this->token]);

        $response->assertJsonStructure([
            "report"=>[],
        ]);

        $response->assertStatus(200);
    }
}
