<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use App\Models\User;
use App\Models\Kendaraan;

class KendaraanTest extends TestCase
{
    
    public $mobil = null;
    public $motor = null;
    public $token = null;

    public function setUp() :void{
        parent::setUp();
        $user       = User::factory()->make([
            'password' => Hash::make("123123"),
        ]);
        $user->save();

        $response = $this->post('/api/login',[
            "email"     => $user->email,
            "password"  => "123123",
        ]);

        $this->mobil = Kendaraan::create(["tahun"=> 2008, "warna" => "silver", "harga" => 1000000, "mesin" => "VVTi", "kapasitas" => 6 , "tipe" => "SUV", "stok"=> 30]);
        $this->motor = Kendaraan::create(["tahun"=> 2020, "warna" => "putih", "harga" => 10000, "mesin" => "125cc", "tipe_suspensi" => "mono" , "tipe_transmisi" => "matic", "stok"=> 50]);
        $this->token = $response['token'];
    }

    /** @test */

    public function test_list()
    {
        $response = $this->get('/api/kendaraan/list',['HTTP_Authorization' => 'Bearer'.$this->token]);

        $response->assertJsonStructure([
            "kendaraan"=>[],
        ]);
        $response->assertStatus(200);
    }

    /** @test */
    public function test_stok_mobil(){
        $response = $this->get('/api/kendaraan/stok/'.$this->mobil["_id"],['HTTP_Authorization' => 'Bearer'.$this->token]);

        $response->assertJsonStructure(["stok"]);
        $response->assertStatus(200);
    }

    /** @test */
    public function test_stok_motor(){

        $response = $this->get('/api/kendaraan/stok/'.$this->motor["_id"],['HTTP_Authorization' => 'Bearer'.$this->token]);

        $response->assertJsonStructure(["stok"]);
        $response->assertStatus(200);
    }
}
