<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Kendaraan;
use App\Models\Penjualan;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $user           = new User();
        $user->name     = "nama";
        $user->email    = "m@mail.com";
        $user->password = Hash::make("123123");
        $user->save();


        $mobil1 = Kendaraan::create(["tahun"=> 2008, "warna" => "silver", "harga" => 1000000, "mesin" => "VVTi", "kapasitas" => 6 , "tipe" => "SUV", "stok"=> 10]);
        $mobil2 = Kendaraan::create(["tahun"=> 2010, "warna" => "hitam", "harga" => 2000000, "mesin" => "Matic", "kapasitas" => 4 , "tipe" => "Mini Bus", "stok"=> 20]);

        $motor1 = Kendaraan::create(["tahun"=> 2020, "warna" => "putih", "harga" => 10000, "mesin" => "125cc", "tipe_suspensi" => "mono" , "tipe_transmisi" => "matic", "stok"=> 10]);
        $motor2 = Kendaraan::create(["tahun"=> 2021, "warna" => "hitam", "harga" => 20000, "mesin" => "150cc", "tipe_suspensi" => "double" , "tipe_transmisi" => "manual", "stok"=> 30]);

        Penjualan::create(["tgl_pembelian"=>date('Y-m-d'), "pembelian"=>[ "kendaraan"=>$mobil1->toArray(), "jumlah"=>1 ],"nama_pembeli"=> "isa"]);
        Penjualan::create(["tgl_pembelian"=>date('Y-m-d'), "pembelian"=>[ "kendaraan"=>$mobil2->toArray(), "jumlah"=>2 ],"nama_pembeli"=> "aji"]);

        Penjualan::create(["tgl_pembelian"=>date('Y-m-d'), "pembelian"=>[ "kendaraan"=>$motor1->toArray(), "jumlah"=>1 ],"nama_pembeli"=> "isa"]);
        Penjualan::create(["tgl_pembelian"=>date('Y-m-d'), "pembelian"=>[ "kendaraan"=>$motor2->toArray(), "jumlah"=>2 ],"nama_pembeli"=> "aji"]);
    }
}
