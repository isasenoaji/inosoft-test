## Requirement
- PHP 8 + php mongodb library
- XAMPP ^v.3.3
- MongoDB Comunty Edition v.4.2
- Composer

## Cara Install
- clone repository ke direktori htdocs dimana XAMPP diinstall
- masuk ke direktori dimana aplikasi di clone dan buka command prompt
- jalankan perintah "composer install"
- sesuaikan koneksi database mongodb Anda dengan kredensial login database MongoDB Anda pada file .env
- jalankan perintah "php artisan db:seed" untuk mengisi data tes
- user default yaitu : email >> m@mail.com dan password >> 123123 (terdapat di file database/seeder)

## Cara pakai
- via unit test : jalankan perintah "php artisan test" maka akan secara otomatis menjalankan fungsi2 yang sudah diatur pada fungsi di file test/feature
- via API : 
    1. login : url('/api/login') >> email & password (POST)
    2. logout : url('/api/logout') >> (GET)
    3. cek list kendaraan : url('/api/kendaraan/list')
    4. cek stok kendaraan : url('/api/kendaraan/{ parameter id kendaraan }')
    5. simpan penjualan : url('/api/penjualan/store') 
        - kendaraan >> id kendaraan
        - jumlah >> jumlah penjualan
        - pembeli >> nama pembeli
        - tgl_pembelian >> tanggal pembelian
        (POST)
    6. rekap penjualan : url('/api/penjualan/report') (POST)
