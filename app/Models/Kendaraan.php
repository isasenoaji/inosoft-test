<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Kendaraan extends Model
{
    use HasFactory;
    protected $collection = 'kendaraan';

    protected $fillable   = [
        "tahun",
        "warna",
        "harga",
        "mesin",
        "tipe_suspensi",
        "tipe_transmisi",
        "kapasitas",
        "tipe",
        "stok",
    ];
}
