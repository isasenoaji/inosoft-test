<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Penjualan extends Model
{
    use HasFactory;
    protected $collection = 'penjualan';

    protected $fillable   = [
        "tgl_transaksi",
        "nama_pembeli",
        "pembelian"
    ];
}
