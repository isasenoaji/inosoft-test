<?php

namespace App\Http\Controllers;

use App\Models\Penjualan;
use Illuminate\Http\Request;
use App\Http\Service\PenjualanService;

class PenjualanController extends Controller
{
    public function store(Request $request){
        $data = (new PenjualanService)->store($request->all());
        
        if(is_object($data)){
            return response()->json([
                "data"      => $data, 
                "message"   => "Success"
            ],200);
        }else{
            return response()->json([
                "data"      => $data, 
                "message"   => "Terjadi kesalahan, kendaraan tidak ditemukan / stok tidak mencukupi"
            ],400);
        }
    }

    public function report(){
        $data = (new PenjualanService)->report();
        return response()->json(["report" => $data]);
    }
}
