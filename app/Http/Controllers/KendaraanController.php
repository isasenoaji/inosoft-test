<?php

namespace App\Http\Controllers;

use App\Models\Kendaraan;
use Illuminate\Http\Request;
use App\Http\Service\KendaraanService;

class KendaraanController extends Controller
{

    public function list(){
        $data = response()->json(["kendaraan"=>(new KendaraanService())->getAll()]);
        return $data;
    }

    public function stok(Request $request, $id){
        $kendaraan = (new KendaraanService())->find($id);
        
        if(!is_null($kendaraan)){
            return response()->json([
                "stok"      => $kendaraan->stok, 
                "message"   => "Success"
            ]);
        }else{
            return response()->json([
                "stok"      => null,
                "message"   => "Kendaraan tidak ada!", 
            ],404);
        }
    }
}
