<?php

namespace App\Http\Service;

use App\Http\Repository\KendaraanRepository;
use App\Http\Repository\PenjualanRepository;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;

class PenjualanService {

    public function store($data){
        try {
            $kendaraan          = $data["kendaraan"];
            $jumlah             = $data["jumlah"];
            $tgl_pembelian      = $data["tgl_pembelian"];
            $pembeli            = $data["pembeli"];

            $kendaraan = ((new KendaraanRepository))->find($kendaraan);

            if(is_null($kendaraan)){
                return null;
            }

            if( $kendaraan->stok - $jumlah >= 0 ){
                $kendaraan->stok -= $jumlah;
                
                if(!(new KendaraanRepository)->update($kendaraan)){
                    return "Terjadi kesalahan pada update stok!!";
                }
                $pembelian = [
                    "kendaraan" => $kendaraan->toArray(), 
                    "jumlah"    => $jumlah
                ];

                $penjualan = (new PenjualanRepository)->store($tgl_pembelian, $pembelian, $pembeli);
                return $penjualan;
            }
            else{
                return "Stok tidak mencukupi!";
            }
        } catch (\Exception $e) {
            return $e->getMessage(); 
        }
        
    }

    public function report(){
        $data = (new PenjualanRepository())->report();

        foreach($data as $report){
            $report->kendaraan = (new KendaraanRepository)->find($report->_id);
        }

        return $data;
    }
}