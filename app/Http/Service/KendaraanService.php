<?php

namespace App\Http\Service;

use App\Http\Repository\KendaraanRepository;
use Tymon\JWTAuth\Facades\JWTAuth;

class KendaraanService {

    public function find($id){
        return (new KendaraanRepository())->find($id);
    }

    public function getAll(){
        return (new KendaraanRepository())->fetchAll();
    }
}