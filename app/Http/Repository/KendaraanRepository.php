<?php

namespace App\Http\Repository;

use App\Models\Kendaraan;
use Tymon\JWTAuth\Facades\JWTAuth;

class KendaraanRepository {

    public function find($id){
        $kendaraan = Kendaraan::where("_id", $id)->first();
        return $kendaraan;
    }
    
    public function fetchAll(){
        $kendaraan = Kendaraan::all();
        return $kendaraan;
    }

    public function update(Kendaraan $kendaraan){
        $_id        = $kendaraan->_id; //separate id from content
        $kendaraan  = $kendaraan->toArray();

        unset($kendaraan["_id"]); //remove index id from content

        if(Kendaraan::where("_id", $_id)->update($kendaraan)){
            return true;
        }
        return false;
    }

}