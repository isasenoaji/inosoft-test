<?php

namespace App\Http\Repository;

use App\Models\Penjualan;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;

class PenjualanRepository {
    public function store($tgl_transaksi, $pembelian, $pembeli){
        return Penjualan::create(["tgl_transaksi"=> $tgl_transaksi,"pembelian"=>$pembelian, "nama_pembeli"=>$pembeli]);
    }

    public function report(){
        $data = Penjualan::raw(function($collection){
            return $collection->aggregate([
                [
                    '$unwind' => '$pembelian',
                ],
                [
                    '$unwind' => '$pembelian.kendaraan'
                ],
                [
                    '$group'  => [
                        "_id"           => '$pembelian.kendaraan._id',
                        "terjual"       => [
                            '$sum'      => '$pembelian.jumlah'
                        ],
                    ]
                ]
            ]);
        });
        return $data;
    }
}