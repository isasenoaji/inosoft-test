<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\KendaraanController;
use App\Http\Controllers\PenjualanController;
use App\Models\Kendaraan;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post("login", [AuthController::class, "login"])->name("login");

Route::middleware("jwt-auth")->group(function(){
    Route::post("logout", [AuthController::class , "logout"]);
    Route::post("refresh-token", [AuthController::class, "refreshToken"]);
    
    Route::prefix("kendaraan")->name("kendaraan-")->group(function(){
        Route::get("/list", [KendaraanController::class, "list"]);
        Route::get("/stok/{id}", [KendaraanController::class, "stok"]);
    });
    
    Route::prefix("penjualan")->group(function(){
        Route::post("/store", [PenjualanController::class, "store"]);
        Route::get("/report", [PenjualanController::class , "report"]);
    });
});
